extern crate hyper;
extern crate futures;
extern crate tokio_core;
extern crate hyper_tls;

use std::time::{SystemTime};
use hyper::{Method, StatusCode, Client};
use hyper::server::{Http, Request, Response, Service};
use hyper::header::{Host};
use hyper_tls::HttpsConnector;
use tokio_core::reactor::Core;
use std::thread;


struct HelloWorld;

impl Service for HelloWorld {
    type Request = Request;
    type Response = Response;
    type Error = hyper::Error;

    type Future = futures::future::FutureResult<Self::Response, Self::Error>;

    fn call(&self, req: Self::Request) -> Self::Future {
        let mut response = Response::new();

        match (req.method(), req.path()) {
            (&Method::Get, "/") => { response.set_body("Echo"); },
            (&Method::Post, "/echo") => { response.set_body(req.body()); }
            _ => { response.set_status(StatusCode::NotFound); }
        };

        futures::future::ok(response)
    }
}

struct RancherProxy;

impl RancherProxy {
    fn new() -> Self {
        return RancherProxy{};
    }
}

impl Service for RancherProxy {
    type Request = Request;
    type Response = Response;
    type Error = hyper::Error;
    type Future = futures::future::FutureResult<Self::Response, Self::Error>;

    fn call(&self, req: Self::Request) -> Self::Future {
        let mut core = match Core::new() {
            Ok(core) => core,
            Err(_) => { println!("Something went wrong initializing the core"); panic!(); },
        };
        let https_connector = match HttpsConnector::new(4, &core.handle()) {
            Ok(connector) => connector,
            Err(err) => { println!("Cannot initialize https connector: {}", err); panic!() }
        };
        let client = Client::configure().connector(https_connector).build(&core.handle());
        print!("{:?} - Input uri: {}", SystemTime::now(), req.uri());
        match req.headers().get::<Host>() {
            Some(host) => println!(" - Host: {}", host),
            None => println!(" - Host header not found, return some error")
        }
        let uri = "https://httpstat.us/200".parse().unwrap();
        let mut reqq = Request::new(req.method().clone(), uri);
        reqq.headers_mut().clone_from(req.headers());
        reqq.headers_mut().set(Host::new("httpstat.us", None));
        let fut_resp = client.request(reqq);
        println!("Running request");
        match core.run(fut_resp) {
            Ok(response) => {
                println!("- {:?} - Request done!", SystemTime::now());
                futures::future::ok(response)
            },
            Err(err) => {
                let mut response = Response::new();
                response.set_status(StatusCode::InternalServerError);
                response.set_body(format!("error: {}", err));

                futures::future::ok(response)
            }
        }
    }
}

fn main() {
    let mut threads = vec![];

    threads.push(
        thread::spawn(|| {
            let addr = "127.0.0.1:3000".parse().unwrap();
            let server = match Http::new().bind(&addr, || Ok(HelloWorld)) {
                Ok(server) => server,
                Err(err) => { println!("Error trying to listen in port 3000: {}", err); panic!(); }
            };
            println!("Listening on 3000");
            server.run().unwrap();
        })
    );
    let proxy = RancherProxy::new();
    threads.push(
        thread::spawn(|| {
            let addr = "127.0.0.1:4000".parse().unwrap();
            let server = Http::new().bind(&addr, || Ok(RancherProxy)).unwrap();
            println!("Listening on 4000");
            server.run().unwrap();
        })
    );

    for thread in threads {
        let _ = thread.join();
    }
}
